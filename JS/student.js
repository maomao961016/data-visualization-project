window.onload = function () {

	// 获取元素
	let addModal = document.querySelector('#addModal') // 模态框
	let tbody = document.querySelector('tbody')
	let btnAddStu = document.querySelector('.btnAddStu') //添加学员按钮
	let btnAddStudent = document.querySelector('.btnAddStudent') // 确认添加按钮
	let addProvinceSelect = document.querySelector('[name="province"]') // 新增省下拉列表
	let addCitySelect = document.querySelector('[name="city"]') // 新增省下拉列表
	let addCountySelect = document.querySelector('[name="county"]') // 新增省下拉列表
	let addModalLabel = document.querySelector('#addModalLabel') // 录入新学员

	// 定义一个全局id
	let id


	// 封装渲染函数
	function render() {
		//  数据渲染
		axios({
			url: '/student/list',

		}).then(res => {
			let htmlStr = ''
			res.data.data.forEach(function (v, index) {
				htmlStr += `<tr>
	        <th scope="row">${index + 1}</th>
	        <td>${v.name}</td>
	        <td>${v.age}</td>
	        <td>${v.sex}</td>
	        <td>${v.group}</td>
	        <td>${v.phone}</td>
	        <td>${v.salary}</td>
	        <td>${v.truesalary}</td>
	        <td>${v.province + v.city + v.county}</td>
	        <td>
	          <button type="button" class="btn btn-primary btn-sm btnedit" data-id='${v.id}'>修改</button>
	          <button type="button" data-id='${v.id}'  class="btn btn-danger btn-sm btndel">删除</button>
	        </td>
	      </tr>`
			})
			tbody.innerHTML = htmlStr
		})
	}
	render()

	
	// 默认加载所有省
	axios({
		url: '/geo/province',
	}).then(res => {
		// 一开始重置市和县区下拉列表
		addCitySelect.innerHTML = `<option selected value="">--市--</option>`
		addCountySelect.innerHTML = `<option selected value="">--县--</option> `
		let htmlStr = `<option selected value="">--省--</option>`
		res.data.forEach(function (value) {
			htmlStr += `<option  value="${value}">--${value}--</option>`
			addProvinceSelect.innerHTML = htmlStr
		})
	})

	// 实现省市区联动
	// 选择省,加载这个市
	// 使用change事件监听是否选择的一个省
	addProvinceSelect.addEventListener('change', function () {
		let pname = this.value
		// 如果用户没有选择具体的省则不去发起axios请求,同时重置市,县区的选项值
		addCitySelect.innerHTML = `<option selected value="">--市--</option>`
		addCountySelect.innerHTML = `<option selected value="">--县--</option> `
		if (pname == '') {
			return
		}
		// 根据用户所选择的省加载它对应的所有市
		axios({
			url: '/geo/city',
			params: { pname }
		}).then(res => {
			let htmlStr = `<option selected value="">--市--</option>`
			res.data.forEach(function (value) {
				htmlStr += `<option  value="${value}">--${value}--</option>`
			})
			addCitySelect.innerHTML = htmlStr
		})

	})

	// 选择市加载它对应的县区
	addCitySelect.addEventListener('change', function () {
		let pname = addProvinceSelect.value
		let cname = this.value
		addCountySelect.innerHTML = '<option selected value="">--县--</option>'
		if (cname == '') {
			return
		}
		axios({
			url: '/geo/county',
			params: { pname, cname }
		}).then(res => {
			let htmlStr = `<option selected value="">--县--</option>`
			res.data.forEach(function (value) {
				htmlStr += `<option  value="${value}">--${value}--</option>`
			})
			addCountySelect.innerHTML = htmlStr
		})

	})

	// 添加校验规则
	// 实现数据的验证
	function test() {
		return {
			fields: {
				name: {
					validators: {
						// 指定自定义的验证规则
						notEmpty: {
							message: '姓名不能为空'
						},
						stringLength: {
							min: 2,
							max: 10,
							message: '姓名长度2~10位'
						}
					}
				},
				age: {
					validators: {
						notEmpty: {
							message: '年龄不能为空'
						},
						greaterThan: {
							// 大于指定的值
							value: 18,
							message: '年龄不能小于18岁'
						},
						lessThan: {
							// 小于指定的值
							value: 60,
							message: '年龄不能超过60岁'
						}
					}
				},
				sex: {
					validators: {
						choice: {
							min: 1,
							max: 1,
							message: '请选择性别'
						}
					}
				},
				group: {
					validators: {
						notEmpty: {
							message: '组号不能为空'
						},
						regexp: {
							regexp: /^\d{1,2}$/,
							message: '请选择有效的组号'
						}
					}
				},
				phone: {
					validators: {
						notEmpty: {
							message: '手机号不能为空'
						},
						regexp: {
							regexp: /^1\d{10}$/,
							message: '请填写有效的手机号'
						}
					}
				},
				salary: {
					validators: {
						notEmpty: {
							message: '实际薪资不能为空'
						},
						greaterThan: {
							value: 800,
							message: '期望薪资不能低于800'
						},
						lessThan: {
							value: 100000,
							message: '期望薪资不能高于100000'
						}
					}
				},
				truesalary: {
					validators: {
						notEmpty: {
							message: '实际薪资不能为空'
						},
						greaterThan: {
							value: 800,
							message: '实际薪资不能低于800'
						},
						lessThan: {
							value: 100000,
							message: '实际薪资不能高于100000'
						}
					}
				},
				province: {
					validators: {
						notEmpty: {
							message: '省份必填'
						}
					}
				},
				city: {
					validators: {
						notEmpty: {
							message: '市必填'
						}
					}
				},
				county: {
					validators: {
						notEmpty: {
							message: '县必填'
						}
					}
				}
			}
		}
	}

	// 点击添加学员按钮弹出模态框
	btnAddStu.addEventListener('click', function () {
		btnAddStudent.innerText = '录入新学员'
		addModalLabel.innerText = '确认添加'
		$('#addModal').modal('show')
	})

	// 添加学员 和 编辑学员信息
	$('.add-form').bootstrapValidator(test()).on('success.form.bv', function (e) {
		e.preventDefault();
		// 通过验证，这里的代码将会执行。我们将Ajax请求的代码放到这里即可
		// 收集表单数据 使用formdata
		let addForm = document.querySelector('.add-form')
		let formdata = new FormData(addForm)
		let data = {}
		formdata.forEach(function (value, key) {
			// console.log(value, key);
			data[key] = value
		})
		// 判断到底时编辑还是新增
		if (id) {
			// 添加id参数
			data.id = id
			//  发起axios请求
			axios({
				url: '/student/update',
				method: 'PUT',
				data
			}).then(res => {
				toastr.success('编辑成功')
				$('#addModal').modal('hide')
				render()
			})
			// 编辑完之后必须将id重置
			id = null
		} else {
			axios({
				url: '/student/add',
				method: 'post',
				data
			}).then(res => {
				toastr.success('添加成功')
				$('#addModal').modal('hide')
				render()

			})
		}
	});

	// 实现学员信息的删除
	tbody.addEventListener('click', function (e) {
		if (e.target.classList.contains('btndel')) {
			if (!confirm('你确定要删除吗?')) return
			let id = e.target.dataset.id
			axios({
				url: '/student/delete',
				method: 'DELETE',
				params: { id }
			}).then(res => {
				toastr.success('删除成功')
				render()
			})
		}


	})

	// 修改业务1: 展示默认数据
	//单击列表中的:"编辑按钮"弹出模态框,同时展示默认数据
	tbody.addEventListener('click', function (e) {
		if (e.target.classList.contains('btnedit')) {
			btnAddStudent.innerText = '确认编辑'
			addModalLabel.innerText = '编辑用户信息'
			// 填充默认数据,将id存储到全局变量
			id = e.target.dataset.id
			// 根据id查询对应的数据
			axios({
				url: '/student/one',
				params: { id }
			}).then(res => {
				let data = res.data.data
				//填充数据
				addModal.querySelector('[name="name"]').value = data.name
				addModal.querySelector('[name="age"]').value = data.age
				addModal.querySelector('[name="group"]').value = data.group
				//性别:需要使用三元表达式,主要是根据数据判断到底为哪一个单选按钮添加checked
				data.sex == '男' ? (addModal.querySelectorAll('[name="sex"]')[0].checked = true) : (addModal.querySelectorAll('[name="sex"]')[1].checked = true)
				addModal.querySelector('[name="phone"]').value = data.phone
				addModal.querySelector('[name="salary"]').value = data.salary
				addModal.querySelector('[name="truesalary"]').value = data.truesalary

				//省的数据一开始就加载好了索引可以用value进行匹配
				addProvinceSelect.value = data.province
				// 城市和县区需要赋值
				addCitySelect.innerHTML = `<option selected value="${data.city}">--${data.city}--</option>`
				addCountySelect.innerHTML = `<option selected value="${data.county}">--${data.county}--</option>`
				// 弹出模态框
				$('#addModal').modal('show')
			})
		}
	})
}