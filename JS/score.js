window.onload = function () {
	let tbody = document.querySelector('tbody')
	axios({
		url: '/score/list'
	}).then(res => {

		let htmlstr = ''
		// 数据是一个对象,所以使用for..in进行遍历
		let data = res.data.data

		for (let key in data) {
			console.log(key, data[key]);
			htmlstr += `<tr data-id ='${key}'>
          <th scope="row">${key}</th>
          <td>${data[key].name}</td>
          <td class="score" data-batch='1'>${data[key].score[0]}</td>
          <td class="score"  data-batch='2'>${data[key].score[1]}</td>
          <td class="score"  data-batch='3'>${data[key].score[2]}</td>
          <td class="score"  data-batch='4'>${data[key].score[3]}</td>
          <td class="score"  data-batch='5'>${data[key].score[4]}</td>
        </tr>`
		}
		tbody.innerHTML = htmlstr
	})

	//单击表格实现成绩的更新和录入--在表格td中添加一个用户用户交互的输入框
	//如果当前td中有成绩,就是更新
	// 如果没有就是录入

	//实现:
	// 1.如果直接失去焦点就是取消操作
	//2.如果按下enter键就是确认当前操作

	tbody.addEventListener('click', function (e) {
		if (e.target.className == 'score') {
			let td = e.target // 当前所点击的td
			// 为了实现后期失去焦点之后还原td的数据,将td的值先存储
			let current = td.innerText
			if (!td.querySelector('input')) {
				// 创建一个输入框
				let scoreInput = document.createElement('input')
				// 给当前创建的输入框赋值
				scoreInput.value = current
				// 设置输入框的样式
				scoreInput.classList.add('inp')
				//将td的内容清空
				td.innerText = ''
				// 添加到当前td中
				td.appendChild(scoreInput)
				//让输入框聚焦
				scoreInput.select()


				// 添加输入框失去焦点事件
				scoreInput.addEventListener('blur', function () {
					//  还原td的数据
					td.innerText = current
				})

				scoreInput.addEventListener('keyup', function (e) {

					if (e.key == 'Enter') {
						// 获取数据
						let stu_id = td.parentNode.dataset.id
						let batch = td.dataset.batch
						let score = scoreInput.value
						// 发起axios请求
						axios({
							url: '/score/entry',
							method: 'POST',
							data: {
								stu_id,
								batch,
								score
							}
						}).then(res => {
							toastr.success('成绩修改或录入成功')
							td.innerHTML = ''
							// scoreInput.remove() // 不能使用remove有坑会报错
							td.innerText = score
						})


					}
				})
			}
		}



	})


















}