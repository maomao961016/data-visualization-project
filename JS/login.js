window.onload = function () {
	//单击去注册按钮
	let goRegister = document.querySelector('.goRegister')
	//单击去登陆按钮
	let goLogin = document.querySelector('.goLogin')
	// 注册页面大盒子
	let register = document.querySelector('.register')
	// 登陆页面大盒子
	let login = document.querySelector('.login')
	// 单击去注册按钮绑定事件
	goRegister.addEventListener('click', function () {
		// 显示注册页面同时隐藏登陆页面
		register.style.display = 'block'
		login.style.display = 'none'

	})
	goLogin.addEventListener('click', function () {
		// 显示登陆页面同时隐藏注册页面
		register.style.display = 'none'
		login.style.display = 'block'

	})
	// 封装校验规则
	function test() {
		return {
			fields: {
				username: {
					// 这里username是 input 的name属性值，表示对这个输入框进行验证
					validators: {
						// 添加真正的校验规则
						notEmpty: {
							//不能为空
							message: '用户名不能为空.' // 如果不满足校验规则，则给出这句提示
						},
						stringLength: {
							//检测长度
							min: 2, // 最少2位
							max: 15, // 最多15位
							message: '用户名需要2~15个字符'
						}
					}
				},
				password: {
					validators: {
						notEmpty: {
							message: '密码不能为空'
						},
						stringLength: {
							//检测长度
							min: 6,
							max: 15,
							message: '密码需要6~15个字符'
						}
					}
				}
			}
		}
	}
	// 单击实现注册
	$('.register form')
		.bootstrapValidator(test())
		.on('success.form.bv', function (e) {
			// 阻止表单默认行为
			e.preventDefault()

			//  获取数据
			let username = register.querySelector('[name="username"]').value
			let password = register.querySelector('[name="password"]').value
			// console.log(username, password);

			// 发起axios请求
			axios({
				url: '/api/register',
				method: 'POST',
				data: { username, password }
			}).then(res => {
				if (res.data.code == 0) {

					// 弹出提示信息
					toastr.success(res.data.message)
					// 切换到登陆页面
					register.style.display = 'none'
					login.style.display = 'block'
					register.querySelector('[name="username"]').value = ''
					register.querySelector('[name="password"]').value = ''
				}
				else {
					toastr.warning(res.data.message)
				}
			})


		})
	// 单击实现登陆
	$('.login form')
		.bootstrapValidator(test())
		.on('success.form.bv', function (e) {
			// 阻止表单默认行为
			e.preventDefault()

			//  获取数据
			let username = login.querySelector('[name="username"]').value
			let password = login.querySelector('[name="password"]').value
			// console.log(username, password);
			// 发起axios请求
			axios({
				url: '/api/login',
				method: 'POST',
				data: { username, password }
			}).then(res => {
				console.log(res.data);
				if (res.data.code == 0) {

					// 弹出提示信息
					toastr.success(res.data.message)
					// 将token存储到本地存储
					localStorage.setItem('mytoken_76', res.data.token)
					// 实现页面跳转
					location.href = './index.html'
				}
				else {
					toastr.warning(res.data.message)
				}
			})


		})


}