window.onload = function () {
	// 获取元素
	// 点我初始化按钮
	let init = document.querySelector('.init')
	// 点击退出按钮
	let logout = document.querySelector('.logout')

	let allA = document.querySelectorAll('.nav li li a')
	//  点击初始化
	init.addEventListener('click', function () {
		axios({
			url: '/init/data',

		}).then(res => {
			if (res.data.code == 1) {
				toastr.warning(res.data.message)
			}

		})

	})
	//点击退出按钮
	logout.addEventListener('click', function () {
		if (!confirm('你确定要退出吗？')) return
		localStorage.removeItem('mytoken_76')
		location.href = './index.html'
	})

	//  实现左侧导航栏切换
	allA.forEach(function (ele, index) {
		ele.addEventListener('click', function () {
			//  排它思想
			document.querySelector('.nav li li a.active').classList.remove('active')
			this.classList.add('active')

		})

	})

}